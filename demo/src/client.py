import time 
import sys

import grpc

import demo_pb2 as demo_pb2
import demo_pb2_grpc as demo_pb2_grpc

if __name__ == "__main__":
    server_ip = sys.argv[1]
    server_port = sys.argv[2]

    channel = grpc.insecure_channel(f"{server_ip}:{server_port}")
    stub = demo_pb2_grpc.DemoStub(channel)

    empty = demo_pb2.Empty()

    print("Calling GetDateTime(Empty())")
    dt1 = stub.GetDateTime(empty)
    print("Result: {}-{}-{} {}:{}:{}\n".format(
        dt1.day, dt1.month, dt1.year, dt1.hour, dt1.minute, dt1.second)
    )
    
    print("Calling GetSquared(Number(3.0))")
    sq1 = stub.GetSquared(demo_pb2.Number(number=3.0))
    print("Result: {}\n".format(sq1.number))

    print("Calling GetSquared(Number(6.0))")
    sq2 = stub.GetSquared(demo_pb2.Number(number=6.0))
    print("Result: {}\n".format(sq2.number))

    print("Waiting for 3 seconds...")
    time.sleep(3)

    print("Calling GetDateTime(Empty())")
    dt2 = stub.GetDateTime(empty)
    print("Result: {}-{}-{} {}:{}:{}\n".format(
        dt2.day, dt2.month, dt2.year, dt2.hour, dt2.minute, dt2.second)
    )

    print("Calling GetLog(Empty())")
    print("Result: ")
    log = stub.GetLog(empty)
    for log_entry in log:
        method_name = log_entry.method_name
        dt = log_entry.timestamp
        print("Called {} at {}-{}-{} {}:{}:{}".format(
        method_name, dt.day, dt.month, dt.year, dt.hour, dt.minute, dt.second)
    ) 