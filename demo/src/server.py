import datetime
from concurrent import futures
import sys

import grpc

import demo_pb2_grpc as demo_pb2_grpc
import demo_pb2 as demo_pb2

class DemoServicer(demo_pb2_grpc.DemoServicer):
    def __init__(self):
        super().__init__()

        # Support the logging call by creating a list of tuples of (MethodName, DateTime)
        self.log = []

    def GetDateTime(self, request, context):
        dt = self._get_current_datetime()
        self.log.append(
            demo_pb2.LogEntry(
                method_name = "GetDateTime", 
                timestamp = dt
            )
        )
        return dt

    def GetSquared(self, request, context):
        self.log.append(
            demo_pb2.LogEntry(
                method_name = "GetSquared", 
                timestamp = self._get_current_datetime()
            )
        )
        return demo_pb2.Number(
            number = request.number ** 2
        )

    def GetLog(self, request, context):
        self.log.append(
            demo_pb2.LogEntry(
                method_name = "GetLog", 
                timestamp = self._get_current_datetime()
            )
        )

        for log_entry in self.log:
            yield log_entry

    def _get_current_datetime(self):
        now = datetime.datetime.now()
        return demo_pb2.DateTime(
            day = now.day,
            month = now.month,
            year = now.year,
            hour = now.hour,
            minute = now.minute,
            second = now.second
        )

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
    demo_pb2_grpc.add_DemoServicer_to_server(
        DemoServicer(), server
    )
    server.add_insecure_port('[::]:{}'.format(sys.argv[1]))
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    serve()